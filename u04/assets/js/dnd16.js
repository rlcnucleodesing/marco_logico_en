try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}



function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x,y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar

};

function end(e) {
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");

};

function enter(e) {
  // e.preventDefault();
  e.target.style.border = '1px dashed #555';
  // e.target.style.borderTop=' 6px solid #00AA00';

};

function leave(e) {
  e.target.style.border = '';

};
function enter2(e) {};

function leave2(e) {};
function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja6') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja7') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja8') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};
function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className = "caja_actividad_marco fill flex-item";
  } catch (e) {}
}
function drop(e) {
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  document.getElementById(elementoArrastrado).className = "caja_actividad_marco fill todo_elcuadro" ;
  document.getElementById(elementoArrastrado).style.width='150px';
  // e.target.className = ' caja_186x25_sinBorder ';
  e.target.className = 'blockDrag';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado
  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {
    var respuesta1 = document.getElementById("caja1").firstChild.nextSibling.id;
    var respuesta2 = document.getElementById("caja2").firstChild.nextSibling.id;
    var respuesta3 = document.getElementById("caja3").firstChild.nextSibling.id;
    var respuesta4 = document.getElementById("caja4").firstChild.nextSibling.id;
    var respuesta5 = document.getElementById("caja5").firstChild.nextSibling.id;
    var respuesta6 = document.getElementById("caja6").firstChild.nextSibling.id;
    var respuesta7 = document.getElementById("caja7").firstChild.nextSibling.id;
    var respuesta8 = document.getElementById("caja8").firstChild.nextSibling.id;

    if ((respuesta1 == "arrastrable2") || (respuesta1 == "arrastrable7")) {
      document.getElementById("caja1").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstChild.nextSibling.classList.add("incorrecto");

    }

    if ((respuesta2 == "arrastrable2")  || (respuesta2 == "arrastrable7")) {
      document.getElementById("caja2").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta3 == "arrastrable1") || (respuesta3 == "arrastrable3")) {
      document.getElementById("caja3").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta4 == "arrastrable1") || (respuesta4 == "arrastrable3")) {
      document.getElementById("caja4").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta5 == "arrastrable5") || (respuesta5 == "arrastrable6")) {
      document.getElementById("caja5").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja5").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta6 == "arrastrable5") || (respuesta6 == "arrastrable6")) {
      document.getElementById("caja6").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja6").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta7 == "arrastrable4") || (respuesta7 == "arrastrable8")) {
      document.getElementById("caja7").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja7").firstChild.nextSibling.classList.add("incorrecto");
    }

    if ((respuesta8 == "arrastrable4") || (respuesta8 == "arrastrable8")) {
      document.getElementById("caja8").firstChild.nextSibling.classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja8").firstChild.nextSibling.classList.add("incorrecto");
    }



  if (valorCorrecto >= 8) {
    var x = document.getElementsByClassName("overly")[0];
    var correcto = document.getElementById("final_ok");
    x.style.display = "block";
    correcto.style.display = "block";
    deleteCookie("Intentos");
  } else {
    const intentosEjercicio = parseInt(getCookie("Intentos"));
    if (intentosEjercicio >= 2) {
      bloquearDragger();
      mostrarOverly();

      todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
      deleteCookie("Intentos");
      document.getElementById("volverAIntentar").remove();
      document.getElementById("chequearRespuesta").remove();
    } else {
      var num = parseInt(getCookie("Intentos")) + 1;
      setCookie("Intentos", num.toString(10));

      mostrarOverly();

      var mensaje = "Try again " + num + "/3";
      todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
      bloquearDragger();
    }
  }
} catch (e) {
  var x = document.getElementsByClassName("overly")[0];
  x.style.display = "block";
  var error = document.getElementById("faltan_x_rellenar");
  error.style.display = "block";
}
}
function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =
  '<div class="caja_actividad" style="min-height:350px">'+
    '<p style="text-align:center"><strong> Main Achievements - Progress </strong></p>'+
    '<div class="" style="min-height: 40%;" id="caja1" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable2" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">Describe the progress towards the achievement of the Outputs and Outcomes.</p>'+
        '</div>'+
    '</div>'+
    '<div class="" style="min-height: 40%;" id="caja2" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable7" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">Provide a narrative/story based on the evidence.</p>'+
        '</div>'+
    '</div>'+
'</div><br>'+
'<br>'+
'<div class="caja_actividad" style="min-height:350px">'+
    '<p style="text-align:center"><strong> Problems identified</strong></p>'+
    '<div class="" style="min-height: 40%;" id="caja3" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable1" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">Provide analysis regarding the importance of any variations.</p>'+
        '</div>'+
    '</div>'+
    '<div class="" style="min-height: 40%;" id="caja4" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable3" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style="text-align: left;">Explain any difference between the expected results and the results achieved.</p>'+
        '</div>'+
    '</div>'+
'</div><br>'+
'<br>'+
'<div class="caja_actividad" style="min-height:350px">'+
    '<p style="text-align:center"><strong> Unexpected results</strong></p>'+
    '<div class="" style="min-height: 40%;" id="caja5" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable6" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">List and explain them clearly with evidence.</p>'+
        '</div>'+
    '</div>'+
    '<div class="" style="min-height: 40%;" id="caja6" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable5" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style="text-align: left;">Add to the LFM if determined to be important for monitoring and generation of subsequent reports.</p>'+
        '</div>'+
    '</div>'+
'</div><br>'+
'<br>'+
'<div class="caja_actividad" style="min-height:350px">'+
    '<p style="text-align:center"><strong> Findings, actions and recommendations</strong></p>'+
    '<div class="" style="min-height: 40%;" id="caja7" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable4" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">Should be organized in terms of key results and evidence.</p>'+
        '</div>'+
    '</div>'+
    '<div class="" style="min-height: 40%;" id="caja8" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
        '<div class="caja_actividad_marco fill todo_elcuadro correcto" style="overflow: hidden; width: 150px;" id="arrastrable8" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
            '<p style=" text-align:left">Explain the lessons learned and improvements.</p>'+
        '</div>'+
    '</div>'+
'</div>';


}
