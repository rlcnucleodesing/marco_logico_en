try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}



function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  // console.log(e.target.id);
  e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar
};

function end(e) {
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");
};

function enter(e) {
  // e.preventDefault();
  e.target.style.border = '1px dashed #555';
};

function enter2(e) {};

function leave2(e) {};

function leave(e) {
  e.target.style.border = '';
};

function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className = "caja_actividad fill flex-item";
  } catch (e) {}
}

function drop(e) {
  // ev.preventDefault();
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  // document.getElementById(elementoArrastrado).setAttribute('draggable',false);
  // document.getElementById(elementoArrastrado).style.width = "250px"
  document.getElementById(elementoArrastrado).className = "caja_actividad_sin_margin fill todo_elcuadro";
  e.target.className = 'blockDrag';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado

  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes

  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado

  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {
    var respuesta1 = document.getElementById("caja1").firstChild.nextSibling.id;
    var respuesta2 = document.getElementById("caja2").firstChild.nextSibling.id;
    var respuesta3 = document.getElementById("caja3").firstChild.nextSibling.id;
    var respuesta4 = document.getElementById("caja4").firstChild.nextSibling.id;

    if (respuesta1 == "arrastrable1") {
      document.getElementById("arrastrable1").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstChild.nextSibling.classList.add("incorrecto");

    }
    if (respuesta2 == "arrastrable4") {
      document.getElementById("arrastrable4").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstChild.nextSibling.classList.add("incorrecto");
    }
    if (respuesta3 == "arrastrable3") {
      document.getElementById("arrastrable3").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstChild.nextSibling.classList.add("incorrecto");
    }
    if (respuesta4 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstChild.nextSibling.classList.add("incorrecto");
    }




    if (valorCorrecto >= 4) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        bloquearDragger();
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {
    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }

}

function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =
  '<tbody>'+
    '<tr>'+
        '<td width="18%" valign="top" bgcolor="#ECF9FF">'+
            '<p align="center"><strong>Criteria</strong></p>'+
        '</td>'+
        '<td width="18%" valign="top" bgcolor="#ECF9FF">'+
            '<p align="center"><strong>Description</strong></p>'+
        '</td>'+
        '<td width="35%" valign="top" bgcolor="#ECF9FF">'+
            '<p align="center"><strong>Sub-Criteria</strong></p>'+
        '</td>'+
        '<td width="29%" valign="top" bgcolor="#ECF9FF">'+
            '<p align="center"><strong>Tools</strong></p>'+
        '</td>'+
    '</tr>'+
    '<tr style="height:150px">'+
        '<td width="18%" valign="top">'+
            '<p><strong>Relevance</strong></p>'+
        '</td>'+
        '<td width="18%" valign="top" class="fondo">'+
            '<div class="blockDrag" style="min-width: 190px; min-height: 90px;" id="caja1" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
                '<div class="caja_actividad_sin_margin fill todo_elcuadro correcto" id="arrastrable1" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
                    '<p style="text-align:left; font-size:11px;">The degree to which the objectives of the project are consistent with the needs of the beneficiaries, the needs of the country, global priorities, and the policies of the partners and donors.</p>'+
                '</div>'+
            '</div>'+
        '</td>'+
        '<td width="35%" valign="top">'+
            '<ul>'+
                '<li>Stakeholder participation</li>'+
                '<li>Strategic coordination and adjustment</li>'+
                '<li>Comparative advantages</li>'+
                '<li>Partnerships</li>'+
            '</ul>'+
        '</td>'+
        '<td width="29%" valign="top">'+
            '<ul>'+
                '<li>Dashboard for the evaluation</li>'+
            '</ul>'+
        '</td>'+
    '</tr>'+
    '<tr style="height:150px">'+
        '<td width="18%" valign="top">'+
            '<p><strong>Effectiveness</strong></p>'+
        '</td>'+
        '<td width="18%" valign="top" class="fondo">'+
            '<div class="blockDrag" style="min-width: 190px; min-height: 90px;" id="caja2" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
                '<div class="caja_actividad_sin_margin fill todo_elcuadro correcto" id="arrastrable4" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
                    '<p style="text-align:left; font-size:11px;">The degree to which the outputs are helping to achieve, have achieved or have contributed to the outcomes of the project.</p>'+
                '</div>'+
            '</div>'+
        '</td>'+
        '<td width="35%" valign="top">'+
            '<ul>'+
                '<li>Development capacity</li>'+
                '<li>Employment for indigenous peoples</li>'+
                '<li>Gender equality</li>'+
                '<li>Rural employment  </li>'+
            '</ul>'+
        '</td>'+
        '<td width="29%" valign="top">'+
            '<ul>'+
                '<li>Dashboard for the evaluation</li>'+
            '</ul>'+
        '</td>'+
    '</tr>'+
    '<tr style="height:150px">'+
        '<td width="18%" valign="top">'+
            '<p><strong>Efficiency</strong></p>'+
        '</td>'+
        '<td width="18%" valign="top" class="fondo">'+
            '<div class="blockDrag" style="min-width: 190px; min-height: 90px;" id="caja3" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
                '<div class="caja_actividad_sin_margin fill todo_elcuadro correcto" id="arrastrable3" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
                    '<p style="text-align:left; font-size:11px;">The degree to which the project is completed on time and at a reasonable cost according to plan, and the defined outputs are achieved.</p>'+
                '</div>'+
            '</div>'+
        '</td>'+
        '<td width="35%" valign="top">'+
            '<ul>'+
                '<li>Work Plan progress</li>'+
                '<li>Percentage of execution of the project budget</li>'+
            '</ul>'+
        '</td>'+
        '<td width="29%" valign="top">'+
            '<ul>'+
                '<li>Monitoring budget</li>'+
                '<li>Risk management plan</li>'+
            '</ul>'+
        '</td>'+
    '</tr>'+
    '<tr style="height:150px">'+
        '<td width="18%" valign="top">'+
            '<p><strong>Sustainability</strong></p>'+
        '</td>'+
        '<td width="18%" valign="top" class="fondo">'+
            '<div class="blockDrag" style="min-width: 190px; min-height: 90px;" id="caja4" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">'+
                '<div class="caja_actividad_sin_margin fill todo_elcuadro correcto" id="arrastrable2" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
                    '<p style="text-align:left; font-size:11px;">The extent to which there will be a continuation of the benefits of a project once it has ended, and the probability of the benefits remaining in the long-term.</p>'+
                '</div>'+
            '</div>'+
        '</td>'+
        '<td width="35%" valign="top">'+
            '<ul>'+
                '<li>During implementation, the focus is on progress compared to the output and outcome indicators, and the extent to which users have access to the benefits and services of the project.</li>'+
            '</ul>'+
        '</td>'+
        '<td width="29%" valign="top">'+
            '<ul>'+
                '<li>Logical Framework Matrix (LFM)</li>'+
                '<li>Stakeholder surveys</li>'+
            '</ul>'+
        '</td>'+
    '</tr>'+
'</tbody>';


}
