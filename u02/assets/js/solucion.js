function todosLosIntentos(mensaje , funcion , mensajeBoton){
  document.getElementById('final_error').innerHTML='<div class="modal-dialog modal-confirm-error">' +
    '<div class="modal-content">' +
      '<div class="modal-header">' +
        '<div class="icon-box">' +
          '<i class="material-icons">&#xE5CD;</i>' +
        '</div>' +
        '<h4 class="modal-title">'+ mensaje+ '</h4>' +
      '</div>' +
      '<div class="modal-footer">' +
        '<button type="button" class="btn btn-danger back" onclick="'+funcion+'">' + mensajeBoton+ '</button>' +
      '</div>' +
    '</div>' +
  '</div>';
}

function botonesNavegacion(){
  document.getElementById("navegacion").innerHTML='<div id="bot_home" title="Home">' +
      '<a href="javascript:window.parent.close()"></a>' +
  '</div>' +
  '<div id="bot_prev" title="Previous">' +
      '<a href="JavaScript:window.parent.PreviousPage()"></a>' +
  '</div>' +
  '<div id="bot_next" title="Next">' +
      '<a href="JavaScript:window.parent.NextPage()"></a>' +
  '</div>';
}
function dobleFunction(){
    respuestaCorrectas();
    quitarOverly();
    botonesNavegacion();
}

function mostrarOverly(){
  var x = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  x.style.display = "block";
  error.style.display = "block";
}
function bloquearDragger(){
  const cajas = getElementsByClass("caja_actividad_off_sinPadding");
  cajas.forEach((item) => {
    item.setAttribute('draggable','false');
    item.firstChild.nextSibling.setAttribute('draggable','false');
    item.firstChild.nextSibling.setAttribute('ondragstart','');
    item.firstChild.nextSibling.setAttribute('ondragend','');
  });
  document.getElementById("base").setAttribute('ondrop','');
}


function deleteCookie(name, path, domain) {
  if (getCookie(name)) document.cookie = name + '=' +
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}
function getElementsByClass(searchClass, node, tag) {
  var classElements = new Array();
  if (node == null)
    node = document;
  if (tag == null)
    tag = '*';
  var els = node.getElementsByTagName(tag);
  var elsLen = els.length;
  var pattern = new RegExp('(^|\\s)' + searchClass + '(\\s|$)');
  for (i = 0, j = 0; i < elsLen; i++) {
    if (pattern.test(els[i].className)) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}
