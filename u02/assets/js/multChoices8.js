try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN') || (getCookie("Intentos") == null)) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}
const op1 = document.getElementById('op1');
const op2 = document.getElementById('op2');
const op3 = document.getElementById('op3');
const op4 = document.getElementById('op4');
const op5 = document.getElementById('op5');
const op6 = document.getElementById('op6');

op1.addEventListener("click",cambiar1);
op2.addEventListener("click",cambiar2);
op3.addEventListener("click",cambiar3);
op4.addEventListener("click",cambiar4);
op5.addEventListener("click",cambiar5);
op6.addEventListener("click",cambiar6);



function cambiar1(){

  if (op1.classList.contains('caja_actividad_off')){
    op1.classList.remove('caja_actividad_off');
    op1.classList.add('caja_actividad');
  }
  else{
    op1.classList.remove('caja_actividad');
    op1.classList.add('caja_actividad_off');
  }
}

function cambiar2(){

  if (op2.classList.contains('caja_actividad_off')){
    op2.classList.remove('caja_actividad_off');
    op2.classList.add('caja_actividad');
  }
  else{
    op2.classList.remove('caja_actividad');
    op2.classList.add('caja_actividad_off');
  }
}

function cambiar3(){

  if (op3.classList.contains('caja_actividad_off')){
    op3.classList.remove('caja_actividad_off');
    op3.classList.add('caja_actividad');
  }
  else{
    op3.classList.remove('caja_actividad');
    op3.classList.add('caja_actividad_off');
  }
}

function cambiar4(){

  if (op4.classList.contains('caja_actividad_off')){
    op4.classList.remove('caja_actividad_off');
    op4.classList.add('caja_actividad');
  }
  else{
    op4.classList.remove('caja_actividad');
    op4.classList.add('caja_actividad_off');
  }
}

function cambiar5(){

  if (op5.classList.contains('caja_actividad_off')){
    op5.classList.remove('caja_actividad_off');
    op5.classList.add('caja_actividad');
  }
  else{
    op5.classList.remove('caja_actividad');
    op5.classList.add('caja_actividad_off');
  }
}
function cambiar6(){

  if (op6.classList.contains('caja_actividad_off')){
    op6.classList.remove('caja_actividad_off');
    op6.classList.add('caja_actividad');
  }
  else{
    op6.classList.remove('caja_actividad');
    op6.classList.add('caja_actividad_off');
  }
}



function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  invisible.style.display = "none";
  error.style.display = "none";
}



function evaluar(){

  var x = "";
  var valorCorrecto = 0;
  var valorMal = 0;


  try {
    var respuesta1 = document.getElementById("op1").classList;
    var respuesta2 = document.getElementById("op2").classList;
    var respuesta3 = document.getElementById("op3").classList;
    var respuesta4 = document.getElementById("op4").classList;
    var respuesta5 = document.getElementById("op5").classList;
    var respuesta6 = document.getElementById("op6").classList;



    if ( respuesta1.contains('caja_actividad') ) {
        valorMal+=1;
    }
    if ( respuesta2.contains('caja_actividad') ) {
        valorMal+=1;
    }

    if (respuesta3.contains('caja_actividad'))  {
        document.getElementById("op3").classList.add("correcto");
        valorCorrecto+=1;
    }
    if ( respuesta4.contains('caja_actividad') ) {
        document.getElementById("op4").classList.add("correcto");
        valorCorrecto+=1;
      // valorMal+=1;
    }
    if ( respuesta5.contains('caja_actividad') ) {
        document.getElementById("op5").classList.add("correcto");
        valorCorrecto+=1;
      // valorMal+=1;
    }
    if ( respuesta6.contains('caja_actividad') ) {
        valorMal+=1;
    }



  if( (valorCorrecto==3) && (valorMal==0) ){
    var x = document.getElementsByClassName("overly")[0];
    var correcto = document.getElementById("final_ok");
    x.style.display="block";
    correcto.style.display="block";
    deleteCookie("Intentos");
  }
  else{
    const intentosEjercicio = parseInt(getCookie("Intentos"));
    if (intentosEjercicio >= 2) {
      mostrarOverly();

      todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
      deleteCookie("Intentos");
      document.getElementById("volverAIntentar").remove();
      document.getElementById("chequearRespuesta").remove();
      inicio();
    } else {
      var num = parseInt(getCookie("Intentos")) + 1;
      setCookie("Intentos", num.toString(10));

      var mensaje = "Try again " + num + "/3";
      todosLosIntentos(mensaje, "quitarOverly()", "Go Back");

      mostrarOverly();
      inicio();
    }
  }
} catch (e) {

}

}

function inicio(){
  document.getElementById("op1").className="caja_actividad_off fill opcion";
  document.getElementById("op2").className="caja_actividad_off fill opcion";
  document.getElementById("op3").className="caja_actividad_off fill opcion";
  document.getElementById("op4").className="caja_actividad_off fill opcion";
  document.getElementById("op5").className="caja_actividad_off fill opcion";
  document.getElementById("op6").className="caja_actividad_off fill opcion";
}

function respuestaCorrectas() {
  document.getElementById("respuesta").innerHTML =


          '<div class="row">'+
            '<div class="col-md-4">'+
              '<div class=" caja_actividad_off fill opcion" id="op1" style="min-height:170px;">'+
                '<p>The focus of technical work on the critical aspects of the project was not clear, enabling the detection of gaps and weaknesses in the interventions.</p>'+
              '</div>'+
            '</div>'+
            '<div class="col-md-4">'+
              '<div class=" caja_actividad_off fill opcion" id="op2" style="min-height:170px;">'+
                '<p>There was no common terminology to facilitate communication, reduce ambiguities when analyzing a problem, help to plan the response and facilitate decision-making.</p>'+
              '</div>'+

            '</div>'+
            '<div class="col-md-4">'+
              '<div class="fill opcion caja_actividad correcto" id="op3" style="min-height:70px;">'+
                '<p>There was no clear picture of what the project would look like if it succeeded, and the evaluators did not have an objective basis to compare what was planned with what was happening in reality.</p>'+
              '</div>'+
            '</div>'+
          '</div>'+

          '<div class="row">'+
            '<div class="col-md-4">'+
              '<div class="fill opcion caja_actividad correcto" id="op4" style="min-height:70px;">'+
                '<p>Project planning lacked precision, contained multiple objectives that were not related to project activities.</p>'+
              '</div>'+
            '</div>'+
            '<div class="col-md-4">'+
              '<div class="fill opcion caja_actividad correcto" id="op5" style="min-height:70px;">'+
                '<p>There were projects that were not executed successfully, where the scope of responsibility of the project manager was not clearly defined.</p>'+
              '</div>'+

            '</div>'+
            '<div class="col-md-4">'+
              '<div class=" caja_actividad_off fill opcion" id="op6" style="min-height:70px;">'+
                '<p>The different aspects of the program or project, which serve as a guide for the entire evaluation process, were not clearly presented.</p>'+
              '</div>'+
            '</div>'+
          '</div>';




}
