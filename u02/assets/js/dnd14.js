try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)) document.cookie = name + '=' +
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}

function getElementsByClass(searchClass, node, tag) {
  var classElements = new Array();
  if (node == null)
    node = document;
  if (tag == null)
    tag = '*';
  var els = node.getElementsByTagName(tag);
  var elsLen = els.length;
  var pattern = new RegExp('(^|\\s)' + searchClass + '(\\s|$)');
  for (i = 0, j = 0; i < elsLen; i++) {
    if (pattern.test(els[i].className)) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}

function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar

};

function end(e) {
  e.target.style.opacity = '1'; // Restaura la opacidad del elemento
  // e.target.style.margin = "4px"
  e.target.style.display = "inline-block"
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");

};

function enter(e) {
  // e.preventDefault();
  e.target.style.border = '1px dashed #555';

};

function enter2(e) {};

function leave(e) {
  e.target.style.border = '';

};

function leave2(e) {};

function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false; // Cualquier elemento se puede soltar en la papelera
  }
};

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className += " flex-item";
  } catch (e) {}
}
function drop(e) {
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  document.getElementById(elementoArrastrado).classList.remove('flex-item');
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};
function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado

  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {
    var respuesta1 = document.getElementById("caja1").firstChild.nextSibling.id;
    var respuesta2 = document.getElementById("caja2").firstChild.nextSibling.id;
    var respuesta3 = document.getElementById("caja3").firstChild.nextSibling.id;
    var respuesta4 = document.getElementById("caja4").firstChild.nextSibling.id;
    var respuesta5 = document.getElementById("caja5").firstChild.nextSibling.id;



    if (respuesta1 == "arrastrable8") {
      document.getElementById("arrastrable8").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstChild.nextSibling.classList.add("incorrecto");

    }

    if (respuesta2 == "arrastrable1") {
      document.getElementById("arrastrable1").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta3 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta4 == "arrastrable5") {
      document.getElementById("arrastrable5").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta5 == "arrastrable7") {
      document.getElementById("arrastrable7").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja5").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (valorCorrecto >= 5) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if(intentosEjercicio >= 2){
        const cajas = getElementsByClass("caja_actividad_off_sinPadding");
        cajas.forEach((item) => {
          item.setAttribute('draggable','false');
          item.firstChild.nextSibling.setAttribute('draggable','false');
          item.firstChild.nextSibling.setAttribute('ondragstart','');
          item.firstChild.nextSibling.setAttribute('ondragend','');
        });
        document.getElementById("base").setAttribute('ondrop','');

        var x = document.getElementsByClassName("overly")[0];
        var error = document.getElementById("final_error");
        x.style.display = "block";
        error.style.display = "block";

        todosLosIntentos("Attempts are over","dobleFunction()","See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      }
      else{
        var num = parseInt(getCookie("Intentos")) +1;
        setCookie("Intentos", num.toString(10));

        var x = document.getElementsByClassName("overly")[0];
        var error = document.getElementById("final_error");
        x.style.display = "block";
        error.style.display = "block";

        var mensaje = "Try again "+ num+"/3";
        todosLosIntentos(mensaje,"quitarOverly()","Go Back");

        const cajas = getElementsByClass("caja_actividad_off_sinPadding");
        cajas.forEach((item) => {
          item.setAttribute('draggable','false');
          item.firstChild.nextSibling.setAttribute('draggable','false');
          item.firstChild.nextSibling.setAttribute('ondragstart','');
          item.firstChild.nextSibling.setAttribute('ondragend','');
        });
        document.getElementById("base").setAttribute('ondrop','');
      }
    }
  } catch (e) {

    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }

}


function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =

              '<h4 style="line-height:2">The selection of stakeholders related to a '+
                '<div class="" id="caja1" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style=""> <div class="proyecto fill correcto" width="168" height="25" alt="proyecto" style="opacity: 1; display: inline-block;" id="arrastrable8" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div> will influence the result of the'+
                '<div class=" " id="caja2" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style=""> <div class="analisis_del_problema fill correcto" width="168" height="25" alt="análisis del problema" style="opacity: 1; display: inline-block;" id="arrastrable1" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div> , since each '+
                '<div class=" " id="caja3" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style=""> <div class="de_actores fill correcto" width="168" height="25" alt="de actores" style="opacity: 1; display: inline-block;" id="arrastrable2" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div> group will have its '+
                '<div class=" " id="caja4" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style=""> <div class="perspectiva_particular fill correcto" width="168" height="25" alt="perspectiva particular" style="opacity: 1; display: inline-block;" id="arrastrable5" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div> on the '+
                '<div class=" " id="caja5" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style=""> <div class="problema_en_cuestion fill correcto" width="168" height="25" alt="problema en cuestión" style="opacity: 1; display: inline-block;" id="arrastrable7" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>. Therefore, it is important to correctly identify '+
                'the main groups of stakeholders. </h4>';

}
