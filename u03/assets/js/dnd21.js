try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN') || (getCookie("Intentos") == null)) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}

function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar
};

function end(e) {
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");
};

function enter(e) {
  e.target.style.border = '1px dashed #555';

};

function enter2(e) {};

function leave2(e) {};

function leave(e) {
  e.target.style.border = '';
};

function over(e) {
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
}

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className = "caja_actividad fill flex-item";
  } catch (e) {}
}

function drop(e) {
  // ev.preventDefault();
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion

  // // Dimensiones del elemento sobre el que se arrastra
  // tamContX = $('#'+e.target.id).width();
  // // console.log(tamContX);
  // tamContY = $('#'+e.target.id).height();
  // console.log(tamContY);
  // // Dimensiones del elemento arrastrado
  // tamElemX = $('#'+elementoArrastrado).width();
  // tamElemY = $('#'+elementoArrastrado).height();
  // console.log(tamElemX);
  // console.log(tamElemY);
  // // Posicion del elemento sobre el que se arrastra
  // posXCont = $(e.target).position().left;
  // posYCont = $(e.target).position().top;
  //
  // // Posicion absoluta del raton
  // x = e.layerX;
  // y = e.layerY;
  // console.log(x);
  // console.log(y);
  //
  // // Si parte del elemento que se quiere mover se queda fuera se cambia las coordenadas para que no sea asi
  // if (posXCont + tamContX <= x + tamElemX){
  //     x = posXCont + tamContX - tamElemX;
  // }
  //
  // if (posYCont + tamContY <= y + tamElemY){
  //     y = posYCont + tamContY - tamElemY;
  // }

  // document.getElementById(elementoArrastrado).style.position="absolute";
  // document.getElementById(elementoArrastrado).style.left=tamContX+"px";
  // document.getElementById(elementoArrastrado).style.top=tamContY+"px";
  // document.getElementById(elementoArrastrado).style.left="0px";
  // document.getElementById(elementoArrastrado).setAttribute('draggable',false);
  document.getElementById(elementoArrastrado).className = "caja_actividad_sin_margin ";
  e.target.className = 'blockDrag';
  e.target.style.marginBotom = '10px';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado

  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes

  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado

  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}





function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {
    var respuesta1 = document.getElementById("caja1").firstElementChild.id;
    var respuesta2 = document.getElementById("caja2").firstElementChild.id;
    var respuesta3 = document.getElementById("caja3").firstElementChild.id;
    var respuesta4 = document.getElementById("caja4").firstElementChild.id;


    if (respuesta1 == "arrastrable4") {
      document.getElementById("arrastrable4").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstElementChild.classList.add("incorrecto");

    }

    if (respuesta2 == "arrastrable3") {
      document.getElementById("arrastrable3").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstElementChild.classList.add("incorrecto");
    }

    if (respuesta3 == "arrastrable1") {
      document.getElementById("arrastrable1").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstElementChild.classList.add("incorrecto");
    }
    if (respuesta4 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstElementChild.classList.add("incorrecto");
    }




    if (valorCorrecto >= 3) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {

      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        bloquearDragger();
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {

    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }

}
function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =

              '<div class="col-md-3">'+
                '<div class=" caja_actividad_marco" style="min-height:330px;">'+
                  '<div class="blockDrag" style="height: 56px;" id="caja1" >'+

                  '<div class="caja_actividad_sin_margin correcto" id="arrastrable4" >'+
                  '<p style="text-align:center"><strong>Quality</strong></p>'+
                '</div></div><br>'+

                  '<p style="text-align:left">A specific dimension of the concept of effectiveness, which refers to the Organization\'s ability to respond consistently, quickly and appropriately to its customers, users or beneficiaries.</p>'+
                '</div>'+

              '</div>'+
              '<div class="col-md-3">'+
                '<div class=" caja_actividad_marco" style="min-height:330px;">'+
                  '<div class="blockDrag" style="height: 56px;" id="caja2" >'+

                  '<div class="caja_actividad_sin_margin correcto" id="arrastrable3" >'+
                  '<p style="text-align:center"><strong>Economy</strong></p>'+
                '</div></div><br>'+

                  '<p style="text-align:left">This relates to the capacity of an institution to adequately generate and mobilize financial resources in pursuit of its institutional mission. </p>'+
                '</div>'+

              '</div>'+

              '<div class="col-md-3">'+
                '<div class=" caja_actividad_marco" style="min-height:330px;">'+
                  '<div class="blockDrag" style="height: 56px;" id="caja3" >'+

                  '<div class="caja_actividad_sin_margin correcto" id="arrastrable1" >'+
                  '<p style="text-align:center"><strong>Effectiveness</strong></p>'+
                '</div></div><br>'+

                  '<p style="text-align:left">This refers to the degree of compliance with the objectives set, that is, to what extent the area, or the institution as a whole, is meeting its objectives, without necessarily considering the resources allocated for this purpose.</p>'+
                '</div>'+

              '</div>'+

              '<div class="col-md-3">'+
                '<div class=" caja_actividad_marco" style="min-height:330px;">'+
                  '<div class="blockDrag" style="height: 56px;" id="caja4" >'+

                  '<div class="caja_actividad_sin_margin correcto" id="arrastrable2" >'+
                  '<p style="text-align:center"><strong>Efficiency</strong></p>'+
                '</div></div><br>'+

                  '<p style="text-align:left">This refers to the execution of actions or services related to the project using the minimum possible resources (at a lower cost, greater efficiency).</p>'+
                '</div>'+

              '</div>';


}
