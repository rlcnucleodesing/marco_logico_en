try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN') || (getCookie("Intentos") == null)) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}
const op1 = document.getElementById('OE1');
const op2 = document.getElementById('OE2');
const op3 = document.getElementById('OE3');
const op4 = document.getElementById('OE4');
const op5 = document.getElementById('OE5');

op1.addEventListener("click",cambiar);
op2.addEventListener("click",cambiar);
op3.addEventListener("click",cambiar);
op4.addEventListener("click",cambiar);
op5.addEventListener("click",cambiar);

function cambiar(e){
  if (e.target.classList.contains('correcto')){
    e.target.classList.remove('correcto');
  }
  else{
    e.target.classList.add('correcto');
  }
  if (e.target.classList.contains('incorrecto')){
    e.target.classList.remove('incorrecto');
  }
}



function quitarOverly(){
    var invisible = document.getElementsByClassName("overly")[0];
    var error = document.getElementById("final_error");
    invisible.style.display="none";
    error.style.display="none";
}

function evaluar(){

  var x = "";
  var valorCorrecto = 0;
  var valorMal = 0;

  try {
    var respuesta1 = document.getElementById("OE1").classList;
    var respuesta2 = document.getElementById("OE2").classList;
    var respuesta3 = document.getElementById("OE3").classList;
    var respuesta4 = document.getElementById("OE4").classList;
    var respuesta5 = document.getElementById("OE5").classList;
    console.log(respuesta1);
    if ( respuesta1.contains('correcto') ) {
      document.getElementById("OE1").classList.add('correcto');
      valorCorrecto+=1;
    }
    if ( respuesta2.contains('correcto') ) {
      document.getElementById("OE2").classList.add("incorrecto");
      valorMal+=1;
    }

    if (respuesta3.contains('correcto'))  {
      document.getElementById("OE3").classList.add("incorrecto");
      valorMal+=1;
    }
    if ( respuesta4.contains('correcto') ) {
      document.getElementById("OE4").classList.add('correcto');
      valorCorrecto+=1;
    }
    if ( respuesta5.contains('correcto') ) {
      document.getElementById("OE5").classList.add("incorrecto");
      valorMal+=1;
    }

  } catch (e) {
  }
  if( (valorCorrecto==2) && (valorMal==0) ){
    var x = document.getElementsByClassName("overly")[0];
    var correcto = document.getElementById("final_ok");
    x.style.display="block";
    correcto.style.display="block";
    deleteCookie("Intentos");
  }
  else{
    const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();

      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");

        mostrarOverly();
        // inicio();
      }
  }
}
function inicio(){
  document.getElementById("OE1").className="foto_centro img-responsive animated flipInX";
  document.getElementById("OE2").className="foto_centro img-responsive animated flipInX";
  document.getElementById("OE3").className="foto_centro img-responsive animated flipInX";
  document.getElementById("OE4").className="foto_centro img-responsive animated flipInX";
  document.getElementById("OE5").className="foto_centro img-responsive animated flipInX";
}
function respuestaCorrectas() {
  document.getElementById("respuesta").innerHTML =

            '<div class="fill" style="">'+
              '<img src="assets/svg/oe1.svg" alt="OE1" name="OE1" class="foto_centro img-responsive animated flipInX correcto" id="OE1">'+
            '</div>'+
            '<div class="fill ">'+
              '<img src="assets/svg/oe2.svg" alt="OE2" name="OE2" class="foto_centro img-responsive" id="OE2">'+
            '</div>'+
            '<div class="fill">'+
              '<img src="assets/svg/oe3.svg" alt="OE3" name="OE3" class="foto_centro img-responsive " id="OE3">'+
            '</div>'+
            '<div class="fill">'+
              '<img src="assets/svg/oe4.svg" alt="OE4" name="OE4" class="foto_centro img-responsive animated flipInX correcto" id="OE4">'+
            '</div>'+
            '<div class="fill">'+
              '<img src="assets/svg/oe5.svg" alt="OE5" name="OE5" class="foto_centro img-responsive " id="OE5">'+
            '</div>';
}
