try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}





function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar

};

function end(e) {
  e.target.classList.remove('invisible');

  e.dataTransfer.clearData("Data");

};

function enter(e) {
  // e.preventDefault();
  e.target.style.border = '1px dashed #555';

};

function enter2(e) {};

function leave2(e) {};

function leave(e) {
  e.target.style.border = '';

};

function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if ((e.target.childElementCount == 0) || (e.target.childElementCount == 1)) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {

    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className += " fill";
  } catch (e) {}
}

function drop(e) {
  // ev.preventDefault();
  // e.target.style.opacity="-1";
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion

  document.getElementById(elementoArrastrado).classList.remove("fill");
  e.target.className = 'blockDrag';
  // e.target.className= 'caja_186x25_sinBorder';
  // e.target.style.marginBotom = '10px';
  // e.target.style.height = '255px';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado
  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;
  var incorrecto = 0;


  try {

    var respuesta1 = document.getElementById("caja1").querySelectorAll("div");
    var respuesta2 = document.getElementById("caja2").firstElementChild.id;
    var respuesta3 = document.getElementById("caja3").firstElementChild.id;
    var respuesta4 = document.getElementById("caja4").firstElementChild.id;

    // var respuesta2 = document.getElementById("caja2").querySelectorAll("div");
    // var respuesta3 = document.getElementById("caja3").querySelectorAll("div");
    // var respuesta4 = document.getElementById("caja4").querySelectorAll("div");

    var cantidadCaja1 = document.getElementById("caja1").querySelectorAll("div").length;
    // var cantidadCaja2 = document.getElementById("caja2").querySelectorAll("div").length;
    // var cantidadCaja3 = document.getElementById("caja3").querySelectorAll("div").length;
    // var cantidadCaja4 = document.getElementById("caja4").querySelectorAll("div").length;

    // var respuesta5 = document.getElementById("caja5").firstChild.nextSibling.id;
    // var respuesta6 = document.getElementById("caja6").firstChild.nextSibling.id;
    // var respuesta7 = document.getElementById("caja7").firstChild.nextSibling.id;
    // var respuesta8 = document.getElementById("caja8").firstChild.nextSibling.id;
    var arreglo1 = [];
    // var arreglo2 = [];
    // var arreglo3 = [];
    // var arreglo4 = [];

    for (i = 0; i < cantidadCaja1; i++) {
        arreglo1[i]=respuesta1[i].id;
    }
    // for (i = 0; i < cantidadCaja2; i++) {
    //     arreglo2[i]=respuesta2[i].id;
    // }
    // for (i = 0; i < cantidadCaja3; i++) {
    //     arreglo3[i]=respuesta3[i].id;
    // }
    // for (i = 0; i < cantidadCaja4; i++) {
    //     arreglo4[i]=respuesta4[i].id;
    // }
    // console.log(arreglo1);
    // respuesta1.forEach((item) => {
    //   arreglo1.push(item.id);
    // });
    // respuesta2.forEach((item) => {
    //   arreglo2.push(item.id);
    // });
    // respuesta3.forEach((item) => {
    //   arreglo3.push(item.id);
    // });
    // respuesta4.forEach((item) => {
    //   arreglo4.push(item.id);
    // });
    // respuesta1
    if (cantidadCaja1 == 2) {
      for (i = 0; i < cantidadCaja1; i++) {
          if (arreglo1[i] == "arrastrable7") {
            document.getElementById("arrastrable7").classList.add("correcto");

            valorCorrecto += 1;
          } else if (arreglo1[i] == "arrastrable4") {
            document.getElementById("arrastrable4").classList.add("correcto");
            valorCorrecto += 1;
          } else {
            document.getElementById(arreglo1[i]).classList.add("incorrecto");
          }
      }

      // arreglo1.forEach((item) => {
      //   if (item == "arrastrable7") {
      //     document.getElementById("arrastrable7").classList.add("correcto");
      //
      //     valorCorrecto += 1;
      //   } else if (item == "arrastrable4") {
      //     document.getElementById("arrastrable4").classList.add("correcto");
      //     valorCorrecto += 1;
      //   } else {
      //     document.getElementById(item).classList.add("incorrecto");
      //   }
      // });
    } else {
      for (i = 0; i < cantidadCaja1; i++) {
          document.getElementById(arreglo1[i]).classList.add("incorrecto");
      }
      // arreglo1.forEach((item) => {
      //   document.getElementById(item).classList.add("incorrecto");
      // });
    }

    if (respuesta2 == "arrastrable6") {
      document.getElementById("arrastrable6").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById(respuesta2).classList.add("incorrecto");
    }
    // respuesta  2
    if (respuesta3 == "arrastrable5") {
      document.getElementById("arrastrable5").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById(respuesta3).classList.add("incorrecto");
    }
    // respuesta  3
    if (respuesta4 == "arrastrable3") {
      document.getElementById("arrastrable3").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById(respuesta4).classList.add("incorrecto");
    }


    // if (cantidadCaja2 == 1) {
    //
    //   arreglo2.forEach((item) => {
    //     if (item == "arrastrable6") {
    //       document.getElementById("arrastrable6").classList.add("correcto");
    //
    //       valorCorrecto += 1;
    //     } else {
    //       document.getElementById(item).classList.add("incorrecto");
    //     }
    //   });
    // } else {
    //   arreglo2.forEach((item) => {
    //     document.getElementById(item).classList.add("incorrecto");
    //   });
    // }
    //
    // if (cantidadCaja3 == 1) {
    //
    //   arreglo3.forEach((item) => {
    //     if (item == "arrastrable5") {
    //       document.getElementById("arrastrable5").classList.add("correcto");
    //
    //       valorCorrecto += 1;
    //     } else {
    //       document.getElementById(item).classList.add("incorrecto");
    //     }
    //   });
    // } else {
    //   arreglo3.forEach((item) => {
    //     document.getElementById(item).classList.add("incorrecto");
    //   });
    // }
    //
    // if (cantidadCaja4 == 1) {
    //
    //
    //   arreglo4.forEach((item) => {
    //     if (item == "arrastrable3") {
    //       document.getElementById("arrastrable3").classList.add("correcto");
    //
    //       valorCorrecto += 1;
    //     } else {
    //       document.getElementById(item).classList.add("incorrecto");
    //     }
    //   });
    // } else {
    //   arreglo4.forEach((item) => {
    //     document.getElementById(item).classList.add("incorrecto");
    //   });
    // }




    if (valorCorrecto >= 5) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        bloquearDragger();
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {
    console.log(e);
    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }

}
function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =

                  '<div style="float:left; background-image:url(assets/svg/logica_proyecto_01.svg); background-position:left top; background-repeat:no-repeat; height:586px; width:430px;">'+
                    '<div style="position: absolute; left: 148px; top: 9px; width: 183px; height: 127px; float: left;" id="caja4" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" class="blockDrag">'+
                    '<div class="logica_proyecto_04 correcto" id="arrastrable3" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>'+
                    '<div style="position: absolute; left: 148px; top: 156px; width: 183px; height: 127px; float: left;" id="caja3" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" class="blockDrag">'+
                    '<div class="logica_proyecto_06 correcto" id="arrastrable5" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>'+
                    '<div style="position: absolute; left: 148px; top: 304px; width: 183px; height: 127px; float: left;" id="caja2" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" class="blockDrag">'+
                    '<div class="logica_proyecto_07 correcto" id="arrastrable6" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>'+
                    '<div style="position: absolute; left: 56px; top: 453px; width: 370px; height: 127px; float: left; display: flex; flex-direction: row;" id="caja1" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" class="blockDrag">'+
                    '<div class="logica_proyecto_08 correcto" id="arrastrable7" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div><div class="logica_proyecto_05 correcto" id="arrastrable4" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>'+
                  '</div>';




}
