try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}




function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x,y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar
};

function end(e) {
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");
};
function enter2(e) {};
function leave2(e) {};
function enter(e) {
  e.target.style.border = '1px dashed #555';
};
function leave(e) {
  e.target.style.border = '';
};
function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja6') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja7') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja8') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja9') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    // document.getElementById(elementoArrastrado).className += " flex-item";
  } catch (e) {}
}

function drop(e) {
  // ev.preventDefault();
  // e.target.style.opacity="-1";
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  e.target.className = 'blockDrag';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado
  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {

    var respuesta1 = document.getElementById("caja1").firstElementChild.id;
    var respuesta2 = document.getElementById("caja2").firstElementChild.id;
    var respuesta3 = document.getElementById("caja3").firstElementChild.id;
    var respuesta5 = document.getElementById("caja5").firstElementChild.id;
    var respuesta7 = document.getElementById("caja7").firstElementChild.id;
    var respuesta9 = document.getElementById("caja9").firstElementChild.id;

    if (respuesta1 == "arrastrable11") {
      document.getElementById("arrastrable11").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstElementChild.classList.add("incorrecto");

    }

    if (respuesta2 == "arrastrable7") {
      document.getElementById("arrastrable7").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstElementChild.classList.add("incorrecto");
    }

    if (respuesta3 == "arrastrable4") {
      document.getElementById("arrastrable4").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstElementChild.classList.add("incorrecto");
    }

    if (respuesta5 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja5").firstElementChild.classList.add("incorrecto");
    }
    if (respuesta7 == "arrastrable12") {
      document.getElementById("arrastrable12").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja7").firstElementChild.classList.add("incorrecto");
    }
    if (respuesta9 == "arrastrable10") {
      document.getElementById("arrastrable10").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja9").firstElementChild.classList.add("incorrecto");
    }



    if (valorCorrecto >= 6) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        bloquearDragger();
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {
    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }

}

function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =
    '<div class="" id="caja1" style="position: absolute; left: 76px; top: 63px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso11 fill correcto" id="arrastrable11" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>' +
    '<div class="" id="caja2" style="position: absolute; left: 339px; top: 63px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso07 fill correcto" id="arrastrable7" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>' +
    '<div class="" id="caja3" style="position: absolute; left: 207px; top: 153px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso04 fill correcto" id="arrastrable4" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>' +
    '<div class="caso01 " id="caja4" style="position:absolute; left:19px; top:246px; width:182px; height:76px; float:left; "></div>' +
    '<div class="" id="caja5" style="position: absolute; left: 207px; top: 246px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso02 fill correcto" id="arrastrable2" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>' +
    '<div class="caso03 " id="caja6" style="position:absolute; left:395px; top:246px; width:182px; height:76px; float:left; "></div>' +
    '<div class="" id="caja7" style="position: absolute; left: 19px; top: 333px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso12 fill correcto" id="arrastrable12" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>' +
    '<div class="caso08 " id="caja8" style="position:absolute; left:207px; top:333px; width:182px; height:76px; float:left; "></div>' +
    '<div class="" id="caja9" style="position: absolute; left: 395px; top: 333px; width: 182px; height: 76px; float: left;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)"><div class="caso10 fill correcto" id="arrastrable10" draggable="true" ondragstart="start(event)" ondragend="end(event)"></div></div>';
}
