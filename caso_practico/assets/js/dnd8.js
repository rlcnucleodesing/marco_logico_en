try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN')) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}





function start(e) {
  var x = e.layerX;
  var y = e.layerY;
  e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
  e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
  e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
  setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar

};

function end(e) {
  e.target.classList.remove('invisible');
  e.dataTransfer.clearData("Data");

};

function enter(e) {
  // e.preventDefault();
  e.target.style.border = '1px dashed #555';

};

function enter2(e) {};

function leave2(e) {};

function leave(e) {
  e.target.style.border = '';

};

function over(e) {
  // e.preventDefault();
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // console.log(id+"ad")
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja6') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'caja7') {
    if (e.target.childElementCount == 0) {
      return false;
    } else {
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};

function drop2(e) {
  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
    document.getElementById(elementoArrastrado).className = "caja_actividad_pequena fill flex-item";
  } catch (e) {}
}

function drop(e) {
  // ev.preventDefault();
  // e.target.style.opacity="-1";
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion

  // document.getElementById(elementoArrastrado).style.verticalAlign="bottom";
  document.getElementById(elementoArrastrado).className = "caja_actividad_pequena fill todo_elcuadro";
  e.target.className = ' fondo blockDrag';
  // e.target.style.marginBotom = '10px';
  // e.target.style.height = '0px';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado
  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};

function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var falta = document.getElementById("faltan_x_rellenar");
  var error = document.getElementById("final_error");
  // console.log(error);
  invisible.style.display = "none";
  falta.style.display = "none";
  error.style.display = "none";
}

function evaluar() {

  var x = "";
  var valorCorrecto = 0;


  try {
    var respuesta2 = document.getElementById("caja2").firstElementChild.id;
    var respuesta4 = document.getElementById("caja4").firstElementChild.id;
    var respuesta5 = document.getElementById("caja5").firstElementChild.id;
    var respuesta7 = document.getElementById("caja7").firstElementChild.id;



    if (respuesta2 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstElementChild.classList.add("incorrecto");
    }


    if (respuesta4 == "arrastrable6") {
      document.getElementById("arrastrable6").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstElementChild.classList.add("incorrecto");
    }
    if (respuesta5 == "arrastrable3") {
      document.getElementById("arrastrable3").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja5").firstElementChild.classList.add("incorrecto");
    }

    if (respuesta7 == "arrastrable5") {
      document.getElementById("arrastrable5").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja7").firstElementChild.classList.add("incorrecto");
    }
    if ((document.getElementById("TipoBeneficiario1").selectedIndex != 0) && (document.getElementById("TipoBeneficiario1").selectedIndex == 4)) {

      if (document.getElementById("TipoBeneficiario1").classList.contains("incorrecto")) {
        document.getElementById("TipoBeneficiario1").classList.remove("incorrecto");
        document.getElementById("TipoBeneficiario1").classList.add("correcto");
        valorCorrecto += 1;
      } else {
        document.getElementById("TipoBeneficiario1").classList.add("correcto");
        valorCorrecto += 1;
      }
    } else {
      document.getElementById("TipoBeneficiario1").classList.add("incorrecto");
    }
    if ((document.getElementById("TipoBeneficiario2").selectedIndex != 0) && (document.getElementById("TipoBeneficiario2").selectedIndex == 1)) {

      if (document.getElementById("TipoBeneficiario2").classList.contains("incorrecto")) {
        document.getElementById("TipoBeneficiario2").classList.remove("incorrecto");
        document.getElementById("TipoBeneficiario2").classList.add("correcto");
        valorCorrecto += 1;
      } else {
        document.getElementById("TipoBeneficiario2").classList.add("correcto");
        valorCorrecto += 1;
      }
    } else {
      document.getElementById("TipoBeneficiario2").classList.add("incorrecto");
    }
    if ((document.getElementById("TipoBeneficiario3").selectedIndex != 0) && (document.getElementById("TipoBeneficiario3").selectedIndex == 1)) {
      if (document.getElementById("TipoBeneficiario3").classList.contains("incorrecto")) {
        document.getElementById("TipoBeneficiario3").classList.remove("incorrecto");
        document.getElementById("TipoBeneficiario3").classList.add("correcto");
        valorCorrecto += 1;
      } else {
        document.getElementById("TipoBeneficiario3").classList.add("correcto");
        valorCorrecto += 1;
      }

    } else {
      document.getElementById("TipoBeneficiario3").classList.add("incorrecto");
    }



    if (valorCorrecto >= 7) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if (intentosEjercicio >= 2) {
        bloquearDragger();
        mostrarOverly();

        todosLosIntentos("Attempts are over", "dobleFunction()", "See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      } else {
        var num = parseInt(getCookie("Intentos")) + 1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again " + num + "/3";
        todosLosIntentos(mensaje, "quitarOverly()", "Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {

    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }
}

function respuestaCorrectas() {
  document.getElementById("base").innerHTML = '';
  document.getElementById("respuestas").innerHTML =
  '<tbody>'+
    '<tr>'+
        '<td width="15%" valign="top" bgcolor="#6699CC">'+
            '<p style="color:#FFF; text-align:center;"><strong>Stakeholder</strong></p>'+
        '</td>'+
        '<td width="15%" valign="top" bgcolor="#6699CC">'+
            '<p style="color:#FFF; text-align:center;"><strong>Problems</strong><br>'+
                '(how they are affected by the problem) </p>'+
        '</td>'+
        '<td width="20%" valign="top" bgcolor="#6699CC">'+
            '<p style="color:#FFF; text-align:center;"><strong>Interests</strong><br>'+
                '(and if possible recommend actions to address them) </p>'+
'        </td>'+
'        <td width="20%" valign="top" bgcolor="#6699CC">'+
'            <p style="color:#FFF; text-align:center;"><strong>Potential</strong><br>'+
'                (capacity and motivation <br>'+
'                to generate changes) </p>'+
'        </td>'+
'        <td width="20%" valign="top" bgcolor="#6699CC">'+
'            <p style="color:#FFF; text-align:center;"><strong>Type of Beneficiary</strong> </p>'+
'        </td>'+
'    </tr>'+
'    <tr>'+
'        <td bgcolor="#6699CC">'+
'            <p style="color:#FFF; text-align:center;"><strong>Agri-food <br>'+
'                    Industry <br>'+
'                    Association</strong></p>'+
'        </td>'+
'        <td>'+
'            <div class="caja_actividad_pequena" style="min-height:118px; max-width:210px;">'+
'                <p style="text-align:left">Some degree of concern for the public image of the industry. High level of concern about the costs associated with regulation.</p>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class="caja_actividad_pequena" style="min-height:118px; max-width:210px;">'+
'                <p style="text-align:left"><strong>Moderate:</strong> Maintain / increase company profits. Lobby political actors in favour of the industry.</p>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class=" fondo blockDrag" id="caja2" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style="">'+
'                <div class="caja_actividad_pequena fill todo_elcuadro correcto" id="arrastrable2" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
'                    <p style="text-align:left"><strong>High:</strong>  Has the technical and financial resources to purchase or use technologies that improve the nutritional quality of their products. Limited motivation and willingness to change.</p>'+
'                </div>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <label for="TipoBeneficiario"></label>'+
'            <select name="TipoBeneficiario" id="TipoBeneficiario1" class="correcto">'+
'                <option value="Otros Socios">Others</option>'+
'            </select>'+
'        </td>'+
'    </tr>'+
'    <tr>'+
'        <td bgcolor="#6699CC">'+
'            <p style="color:#FFF; text-align:center;"><strong>Ministry of <br>'+
'                    Health</strong></p>'+
'        </td>'+
'        <td>'+
'            <div class="caja_actividad_pequena" style="min-height:118px; max-width:210px;">'+
'                <p style="text-align:left">Limited resources and little technical staff to design and develop policies for the prevention of obesity and malnutrition in the population.</p>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class=" fondo blockDrag" id="caja4" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style="">'+
'                <div class="caja_actividad_pequena fill todo_elcuadro correcto" id="arrastrable6" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
'                    <p style="text-align:left"><strong>High:</strong> Ensure the health of the population and prevent an increase in malnutrition rates due to overweight.</p>'+
'                </div>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class=" fondo blockDrag" id="caja5" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style="">'+
'                <div class="caja_actividad_pequena fill todo_elcuadro correcto" id="arrastrable3" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
'                    <p style="text-align:left"><strong>High:</strong> Potential for proposing new legislation, and moderating negotiations between parties. Ability to implement policies.</p>'+
'                </div>'+
'            </div>'+
'        </td>'+
'        <td><label for="TipoBeneficiario"></label>'+
'            <select name="TipoBeneficiario" id="TipoBeneficiario2" class="correcto">'+
'                <option value="Beneficiarios Directos">Direct beneficiaries</option>'+
'            </select></td>'+
'    </tr>'+
'    <tr>'+
'        <td bgcolor="#6699CC">'+
'            <p style="color:#FFF; text-align:center;"><strong>Ministry of <br>'+
'                    Economy and <br>'+
'                    Finance</strong></p>'+
'        </td>'+
'        <td>'+
'            <div class="caja_actividad_pequena" style="min-height:118px; max-width:210px;">'+
'                <p style="text-align:left">Higher % of GDP allocated to costs derived from obesity.</p>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class=" fondo blockDrag" id="caja7" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)" style="">'+
'                <div class="caja_actividad_pequena fill todo_elcuadro correcto" id="arrastrable5" draggable="true" ondragstart="start(event)" ondragend="end(event)">'+
'                    <p style="text-align:left"><strong>High:</strong> Promote economic and social growth, by generating jobs, increasing productivity and reducing absenteeism.</p>'+
'                </div>'+
'            </div>'+
'        </td>'+
'        <td>'+
'            <div class="caja_actividad_pequena" style="min-height:118px; max-width:210px;">'+
'                <p style="text-align:left"><strong>High:</strong> Provide financial resources to implement policies and regulations that favour healthy nutrition and sustained behavioural changes.</p>'+
'            </div>'+
'        </td>'+
'        <td><label for="TipoBeneficiario"></label>'+
'            <select name="TipoBeneficiario" id="TipoBeneficiario3" class="correcto">'+
'                <option value="Beneficiarios Directos">Direct beneficiaries</option>'+
'            </select></td>'+
'    </tr>'+
'</tbody>';


}
