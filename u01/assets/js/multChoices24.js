try {
  if ( (getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN') || (getCookie("Intentos") == null) ) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {}
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)) document.cookie = name + '=' +
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp('(^|\\s)'+searchClass+'(\\s|$)');
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}

const op1 = document.getElementById('op1');
const op2 = document.getElementById('op2');
const op3 = document.getElementById('op3');

op1.addEventListener("click", cambiar1);
op2.addEventListener("click", cambiar2);
op3.addEventListener("click", cambiar3);
function cambiar1() {

  if (op1.classList.contains('caja_actividad_off')) {
    op1.classList.remove('caja_actividad_off');
    op1.classList.add('caja_actividad');
  } else {
    op1.classList.remove('caja_actividad');
    op1.classList.add('caja_actividad_off');
  }
}
function cambiar2() {

  if (op2.classList.contains('caja_actividad_off')) {
    op2.classList.remove('caja_actividad_off');
    op2.classList.add('caja_actividad');
  } else {
    op2.classList.remove('caja_actividad');
    op2.classList.add('caja_actividad_off');
  }
}

function cambiar3() {

  if (op3.classList.contains('caja_actividad_off')) {
    op3.classList.remove('caja_actividad_off');
    op3.classList.add('caja_actividad');
  } else {
    op3.classList.remove('caja_actividad');
    op3.classList.add('caja_actividad_off');
  }
}
function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  invisible.style.display = "none";
  error.style.display = "none";
}
function evaluar() {

  var x = "";
  var valorCorrecto = 0;
  var valorMal = 0;
  try {
    var respuesta1 = document.getElementById("op1").classList;
    var respuesta2 = document.getElementById("op2").classList;
    var respuesta3 = document.getElementById("op3").classList;



    if (respuesta1.contains('caja_actividad')) {

      valorMal += 1;
    }
    if (respuesta2.contains('caja_actividad')) {
      // document.getElementById("op2").classList.add("correcto");
      valorMal += 1;
    }

    if (respuesta3.contains('caja_actividad')) {
      document.getElementById("op3").classList.add("correcto");
      valorCorrecto += 1;
    }



    if ((valorCorrecto == 1) && (valorMal == 0)) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");
    } else {

      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if(intentosEjercicio >= 2){
        mostrarOverly();

        todosLosIntentos("Attempts are over","dobleFunction()","See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
        inicio();
      }
      else{
        var num = parseInt(getCookie("Intentos")) +1;
        setCookie("Intentos", num.toString(10));

        var mensaje = "Try again "+ num+"/3";
        todosLosIntentos(mensaje,"quitarOverly()","Go Back");

        mostrarOverly();
        inicio();
      }
    }
  } catch (e) {
  }
}

function inicio() {
  document.getElementById("op1").className = "caja_actividad_off fill opcion";
  document.getElementById("op2").className = "caja_actividad_off fill opcion";
  document.getElementById("op3").className = "caja_actividad_off fill opcion";
}

function respuestaCorrectas(){
  document.getElementById("base").innerHTML='';
  document.getElementById("respuestas").innerHTML=
              '<div class="col-md-4">' +
                '<div class=" caja_actividad_off fill opcion" id="op1" style="min-height:150px;">' +
                  '<p>It is a mechanism that allows FAO to coordinate biennial work at all levels (global, regional and country) and establishes the basis for accountability.</p>' +
                '</div>' +
              '</div>' +
              '<div class="col-md-4">' +
                '<div class=" caja_actividad_off fill opcion" id="op2" style="min-height:150px;">' +
                  '<p>It is a biennial operational planning tool that details how the FAO strategy will be implemented.</p>' +
                '</div>' +

              '</div>' +
              '<div class="col-md-4">' +
                '<div class="fill opcion caja_actividad correcto" id="op3" style="min-height:150px;">' +
                  '<p>It is a national planning document that defines the priorities for collaboration between FAO and the government.</p>' +
                '</div>' +
              '</div>';

}
