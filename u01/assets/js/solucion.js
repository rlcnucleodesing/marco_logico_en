function todosLosIntentos(mensaje , funcion , mensajeBoton){
  document.getElementById('final_error').innerHTML='<div class="modal-dialog modal-confirm-error">' +
    '<div class="modal-content">' +
      '<div class="modal-header">' +
        '<div class="icon-box">' +
          '<i class="material-icons">&#xE5CD;</i>' +
        '</div>' +
        '<h4 class="modal-title">'+ mensaje+ '</h4>' +
      '</div>' +
      '<div class="modal-footer">' +
        '<button type="button" class="btn btn-danger back" onclick="'+funcion+'">' + mensajeBoton+ '</button>' +
      '</div>' +
    '</div>' +
  '</div>';
}

function botonesNavegacion(){
  document.getElementById("navegacion").innerHTML='<div id="bot_home" title="Home">' +
      '<a href="javascript:window.parent.close()"></a>' +
  '</div>' +
  '<div id="bot_prev" title="Previous">' +
      '<a href="JavaScript:window.parent.PreviousPage()"></a>' +
  '</div>' +
  '<div id="bot_next" title="Next">' +
      '<a href="JavaScript:window.parent.NextPage()"></a>' +
  '</div>';
}
function dobleFunction(){
    respuestaCorrectas();
    quitarOverly();
    botonesNavegacion();
}

function mostrarOverly(){
  var x = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  x.style.display = "block";
  error.style.display = "block";
}
function bloquearDragger(){
  const cajas = getElementsByClass("caja_actividad_off_sinPadding");
  cajas.forEach((item) => {
    item.setAttribute('draggable','false');
    item.firstChild.nextSibling.setAttribute('draggable','false');
    item.firstChild.nextSibling.setAttribute('ondragstart','');
    item.firstChild.nextSibling.setAttribute('ondragend','');
  });
  document.getElementById("base").setAttribute('ondrop','');
}
