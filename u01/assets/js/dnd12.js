try {
  if ((getCookie("Intentos") == undefined) || (getCookie("Intentos") == 'NaN') || (getCookie("Intentos") == null) ) {
    setCookie("Intentos", "0", 1);
  }
  if (getCookie("Intentos") == "0") {

  }
} catch (e) {

}

function getCookie(name) {
  var start = document.cookie.indexOf(name + "=");
  var len = start + name.length + 1;
  if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
  }
  if (start == -1) return null;
  var end = document.cookie.indexOf(';', len);
  if (end == -1) end = document.cookie.length;
  return unescape(document.cookie.substring(len, end));
}

function setCookie(name, value, expires, path, domain, secure) {
  var today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  var expires_date = new Date(today.getTime() + (expires));
  document.cookie = name + '=' + escape(value) +
    ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ((secure) ? ';secure' : '');
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)) document.cookie = name + '=' +
    ((path) ? ';path=' + path : '') +
    ((domain) ? ';domain=' + domain : '') +
    ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}

function getElementsByClass(searchClass, node, tag) {
  var classElements = new Array();
  if (node == null)
    node = document;
  if (tag == null)
    tag = '*';
  var els = node.getElementsByTagName(tag);
  var elsLen = els.length;
  var pattern = new RegExp('(^|\\s)' + searchClass + '(\\s|$)');
  for (i = 0, j = 0; i < elsLen; i++) {
    if (pattern.test(els[i].className)) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}

function start(e) {
  try {
    var x = e.layerX;
    var y = e.layerY;

    e.dataTransfer.effecAllowed = 'move'; // Define el efecto como mover
    e.dataTransfer.setData("Data", e.target.id); // Coje el elemento que se va a mover
    e.dataTransfer.setDragImage(e.target, x, y); // Define la imagen que se vera al ser arrastrado el elemento y por donde se coje el elemento que se va a mover (el raton aparece en la esquina sup_izq con 0,0)
    // e.target.style.opacity = '-1'; // Establece la opacidad del elemento que se va arrastrar
    setTimeout(() => (e.target.className += ' invisible'), 0); // Establece la opacidad del elemento que se va arrastrar

  } catch (e) {}
};

function end(e) {
  try {
    e.target.classList.remove('invisible');
    e.dataTransfer.clearData("Data");
  } catch (e) {

  }
};

function enter(e) {
  e.target.style.border = '1px dashed #555';

};

function enter2(e) {};

function leave(e) {
  e.target.style.border = '';
};

function leave2(e) {};

function over(e) {
  var elemArrastrable = e.dataTransfer.getData("Data"); // Elemento arrastrado
  var id = e.target.id; // Elemento sobre el que se arrastra
  // return false para que se pueda soltar
  if (id == 'caja1') {
    if (e.target.childElementCount == 0){
        return false;
    }
    else{
      return true;
    }
  }

  if (id == 'caja2') {
    if (e.target.childElementCount == 0){
        return false;
    }
    else{
      return true;
    }
  }

  if (id == 'caja3') {
    if (e.target.childElementCount == 0){
        return false;
    }
    else{
      return true;
    }
  }

  if (id == 'caja4') {
    if (e.target.childElementCount == 0){
        return false;
    }
    else{
      return true;
    }
  }

  if (id == 'caja5') {
    if (e.target.childElementCount == 0){
        return false;
    }
    else{
      return true;
    }
  }
  if (id == 'base') {
    return false;
  }
};

function drop2(e) {

  try {
    var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
    e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  } catch (e) {
    // console.log(e);
  }

}

function drop(e) {
  // ev.preventDefault();
  var elementoArrastrado = e.dataTransfer.getData("Data"); // Elemento arrastrado
  e.target.appendChild(document.getElementById(elementoArrastrado)); // Añade el elemento arrastrado al elemento desde el que se llama a esta funcion
  document.getElementById(elementoArrastrado).className = "caja_actividad_sin_margin fill";
  e.target.className = 'caja_actividad_off_sinPadding';
  e.target.style.marginBotom = '10px';
  e.target.style.border = ''; // Quita el borde del cuadro al que se mueve
};

function clonar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.style.opacity = ''; // Dejamos la opacidad a su estado anterior para copiar el elemento igual que era antes
  var movecarclone = elementoArrastrado.cloneNode(true); // Se clona el elemento
  movecarclone.id = "ElemClonado" + contador; // Se cambia el id porque tiene que ser unico
  contador += 1;
  elementoClonado.style.position = "static"; // Se posiciona de forma "normal" (Sino habria que cambiar las coordenadas de la posición)
  e.target.appendChild(movecarclone); // Se añade el elemento clonado
  e.target.style.border = ''; // Quita el borde del "cuadro clonador"
};

function eliminar(e) {
  var elementoArrastrado = document.getElementById(e.dataTransfer.getData("Data")); // Elemento arrastrado
  elementoArrastrado.parentNode.removeChild(elementoArrastrado); // Elimina el elemento
  e.target.style.border = ''; // Quita el borde
};
function quitarOverly() {
  var invisible = document.getElementsByClassName("overly")[0];
  var error = document.getElementById("final_error");
  var falta_rellenar = document.getElementById("faltan_x_rellenar");
  invisible.style.display = "none";
  error.style.display = "none";
  falta_rellenar.style.display = "none";
}
function evaluar() {
  var x = "";
  var valorCorrecto = 0;
  try {
    var respuesta1 = document.getElementById("caja1").firstChild.nextSibling.id;
    var respuesta2 = document.getElementById("caja2").firstChild.nextSibling.id;
    var respuesta3 = document.getElementById("caja3").firstChild.nextSibling.id;
    var respuesta4 = document.getElementById("caja4").firstChild.nextSibling.id;
    var respuesta5 = document.getElementById("caja5").firstChild.nextSibling.id;
    if (respuesta1 == "arrastrable4") {
      document.getElementById("arrastrable4").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja1").firstChild.nextSibling.classList.add("incorrecto");

    }

    if (respuesta2 == "arrastrable3") {
      document.getElementById("arrastrable3").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja2").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta3 == "arrastrable2") {
      document.getElementById("arrastrable2").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja3").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta4 == "arrastrable5") {
      document.getElementById("arrastrable5").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja4").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (respuesta5 == "arrastrable1") {
      document.getElementById("arrastrable1").classList.add("correcto");
      valorCorrecto += 1;
    } else {
      document.getElementById("caja5").firstChild.nextSibling.classList.add("incorrecto");
    }

    if (valorCorrecto >= 5) {
      var x = document.getElementsByClassName("overly")[0];
      var correcto = document.getElementById("final_ok");
      x.style.display = "block";
      correcto.style.display = "block";
      deleteCookie("Intentos");

    }
    else {
      const intentosEjercicio = parseInt(getCookie("Intentos"));
      if(intentosEjercicio >= 2){
        bloquearDragger();
        mostrarOverly();
        todosLosIntentos("Attempts are over","dobleFunction()","See correct answer");
        deleteCookie("Intentos");
        document.getElementById("volverAIntentar").remove();
        document.getElementById("chequearRespuesta").remove();
      }
      else{
        var num = parseInt(getCookie("Intentos")) +1;
        setCookie("Intentos", num.toString(10));

        mostrarOverly();

        var mensaje = "Try again "+ num+"/3";
        todosLosIntentos(mensaje,"quitarOverly()","Go Back");
        bloquearDragger();
      }
    }
  } catch (e) {
    var x = document.getElementsByClassName("overly")[0];
    x.style.display = "block";
    var error = document.getElementById("faltan_x_rellenar");
    error.style.display = "block";
  }
}

function respuestaCorrectas(){
  document.getElementById("base").innerHTML='';
  document.getElementById("respuestas").innerHTML='<div class="caja_actividad_off_sinPadding" id="caja1" style="min-height: 96px;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">' +
            '<div class="caja_actividad_sin_margin fill correcto" id="arrastrable4" style="min-height:96px;" draggable="false" >' +
                '<p>Contribute to the eradication of hunger, food insecurity and malnutrition</p>' +
              '</div></div>' +
            '<div class="caja_actividad_off_sinPadding" id="caja2" style="min-height: 96px;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">' +
            '<div class="caja_actividad_sin_margin fill correcto" id="arrastrable3" style="min-height:96px;" draggable="false" >' +
                '<p>Make agriculture, forestry and fishing more productive and sustainable</p>' +
              '</div></div>' +
            '<div class="caja_actividad_off_sinPadding" id="caja3" style="min-height: 96px;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">' +
            '<div class="caja_actividad_sin_margin fill correcto" id="arrastrable2" style="min-height: 96px;" draggable="false" >' +
                '<p>Reduce rural poverty</p>' +
              '</div></div>' +
            '<div class="caja_actividad_off_sinPadding" id="caja4" style="min-height: 96px;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave=" leave(event)">' +
            '<div class="caja_actividad_sin_margin fill correcto" id="arrastrable5" style="min-height: 96px;" draggable="false" >' +
                '<p style="">Create agriculture and food systems that are more inclusive and efficient</p>' +
              '</div></div>' +
            '<div class="caja_actividad_off_sinPadding" id="caja5" style="min-height: 96px;" ondragenter="return enter(event)" ondragover="return over(event)" ondrop="return drop(event)" ondragleave="leave(event)">' +
            '<div class="caja_actividad_sin_margin fill correcto" id="arrastrable1" style="min-height:96px;" draggable="false" >' +
                '<p>Increase resilience of livelihoods in the face of crises</p>' +
              '</div></div>';
}
